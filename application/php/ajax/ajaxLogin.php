<?php
ini_set('error_reporting', 'E_ALL & ~E_NOTICE');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

include '../../../sql/MySqlUsers.php';
include '../../../sql/MySqlTokens.php';
include '../auth/generateToken.php';

$enteredLogin = $_POST['login'];
$enteredPassword = $_POST['password'];
$enteredLogin = mysqli_real_escape_string($dbh, $enteredLogin);
$enteredPassword = mysqli_real_escape_string($dbh, $enteredPassword);
$newToken = generate();

$dbLoginPassword = mysqli_query($dbh, "SELECT * FROM users WHERE `user` = '$enteredLogin'")
or die(mysqli_error($dbh));

$result = mysqli_fetch_array($dbLoginPassword);
if (!empty($enteredLogin) AND !empty($enteredPassword)) {
    if ($result['user'] == $enteredLogin AND password_verify($enteredPassword, $result['password'])) {
        $userId = $result['user_id'];

        mysqli_query($dbh, "INSERT INTO tokens (`token`, `userId`)
        VALUES ('$newToken', '$userId')");

        $response = [
            'status' => 200,
            'authStatus' => true,
            'token' => $newToken
        ];

    } else {
        $response = [
            'status' => 0,
            'authStatus' => false,
            'token' => null
        ];
    };
} else {
    $response = [
        'status' => 0,
        'authStatus' => false,
        'token' => null
    ];
}

echo json_encode($response);
?>