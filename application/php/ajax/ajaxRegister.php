<?php
ini_set('error_reporting', 'E_ALL & ~E_NOTICE');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

include '../../../sql/MySqlUsers.php';
include '../../../sql/MySqlTokens.php';
include '../auth/generateToken.php';

$maxId = mysqli_query($dbh, "SELECT MAX(user_id) as maxUserId FROM `users`");
$row = mysqli_fetch_array($maxId);

$userId = $row['maxUserId'] + 1;
$login = $_POST['login'];
$password = password_hash($_POST['password'], PASSWORD_DEFAULT);
$login = mysqli_real_escape_string($dbh, $login);
$password = mysqli_real_escape_string($dbh, $password);
$newToken = generate();

$select = mysqli_query($dbh, "SELECT * FROM new_database.users WHERE `user` = '$login'");
$arr = mysqli_fetch_array($select);

if ($login != $arr['user'] AND !empty($login) AND !empty($password)) {
    $addToDb = mysqli_query($dbh, "INSERT INTO new_database.users (`user`, `password`, `user_id`)
    VALUE ('$login', '$password', $userId)");

    mysqli_query($dbh, "INSERT INTO tokens (`token`, `userId`)
    VALUES ('$newToken', '$userId')");

    $response = [
        'status' => 200,
        'authStatus' => true,
        'token' => $newToken
    ];
} elseif (empty($login) OR empty($password)) {
    $response = [
        'status' => 0,
        'authStatus' => false,
        'token' => null
    ];
} else {
    $response = [
        'status' => 0,
        'authStatus' => false,
        'token' => null
    ];
};
echo json_encode($response);
?>