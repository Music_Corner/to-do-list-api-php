<?php
ini_set('error_reporting', 'E_ALL & ~E_NOTICE');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

include '../../../sql/MySql.php';
include '../auth/authCheck.php';

$frontToken = $_POST['token'];

if (authCheck($frontToken)) {
    $userIdQuery = mysqli_query($dbh, "SELECT userId FROM tokens
    WHERE token = '$frontToken'");
    $userId = mysqli_fetch_array($userIdQuery)['userId'];

    $input = $_POST['text'];
    $dataIndex = $_POST['dataindex'];
    $input = mysqli_real_escape_string($dbh, $input);
    $addToDb = mysqli_query($dbh, "INSERT INTO `new_database`.`table` (`TextTable`, `UserId`, `ID`)
    VALUES ('$input', '$userId', '$dataIndex')");

    $response = [
        'status' => 200,
        'authStatus' => true,
    ];

} else if(authCheck($frontToken) == false) {
    $response = [
        'status' => 0,
        'authStatus' => false,
    ];
}

echo json_encode($response);

?>