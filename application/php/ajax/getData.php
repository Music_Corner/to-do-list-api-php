<?php
ini_set('error_reporting', 'E_ALL & ~E_NOTICE');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

include '../../../sql/MySql.php';
include '../auth/authCheck.php';

$frontToken = $_POST['token'];

if (authCheck($frontToken)) {
    $userIdQuery = mysqli_query($dbh, "SELECT userId FROM tokens
    WHERE token = '$frontToken'");
    $userId = mysqli_fetch_array($userIdQuery)['userId'];

    $select =  mysqli_query($dbh, "SELECT * FROM new_database.`table` WHERE `UserId` = '$userId'");
    $data = null;
    while($row = mysqli_fetch_array($select, MYSQLI_ASSOC)) {
        $data[] = [
            'text' => $row['TextTable'],
            'marked' => boolval($row['Mark']) ? true: false,
            'id' => $row['ID']
        ];
    }

    $response = [
        'authStatus' => true,
        'status' => 200,
        'data' => $data
    ];
} else {
    $response = [
        'authStatus' => false,
        'status' => 0,
    ];
}

echo json_encode($response);
?>
